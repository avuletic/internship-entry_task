# How to start the project:
## Starting backend:
First you need to download and install SQLExpress. You can do so by following this link: https://www.microsoft.com/en-us/sql-server/sql-server-editions-express
### Running migrations
In order to create the database you will need to run the database migrations. In order to accomplish this do the following:
#### Step one (set models as the startup project):
![alt text](https://i.imgur.com/Eln6hWa.png)
#### Step two (open the package console manager):
![alt text](https://i.imgur.com/CwFaQWP.png)
#### Step three (running migrations in package console manager):

Make sure the default project is Phonebook.Models then run the following command:

    update-database

![alt text](https://i.imgur.com/obJ7XWW.png)

#### Step four:

Set the startup project to Phonebook.Web.API and you can build/Debug (F5) the solution.

## Starting frontend:

In order to start the frontend you will need to download all of the node modules. You can do this by navigating to the downloaded project using the console/command prompt(windows) then typing in:

( You can accomplish the same thing by opening the Frontend/Phonebook using visual studio code then opening an terminal and typing in the command )

    npm install

This command will download all the dependencies and devdependencies located in the package.json file.



The project is made using AngularCLI. To start the project you simply use:

    ng serve


## Information about the project:

### Backend architecture:
![alt text](https://i.imgur.com/0mphtbD.png)
### Database model:
![alt text](https://i.imgur.com/zggtZna.png)