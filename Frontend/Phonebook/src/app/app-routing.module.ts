import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookmarkComponent } from './Components/bookmark/bookmark.component';
import { AddcontactComponent } from './Components/addcontact/addcontact.component';
import { EditcontactComponent } from './Components/editcontact/editcontact.component';
import { HomeComponent } from './Components/home/home.component';
import { SearchComponent } from './Components/search/search.component';

const routes: Routes = [
  {path:'', component : HomeComponent},
  {
    path:'search',
    component : HomeComponent,
  },
  {
    path:'name/:value',
    component: SearchComponent
  },
  {
    path:'surname/:value',
    component: SearchComponent
  },
  {
    path:'tag/:value',
    component: SearchComponent
  },
  {path:'edit/:id',component: EditcontactComponent},
  {path:'bookmark', component : BookmarkComponent},
  {path:'addcontact', component: AddcontactComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
