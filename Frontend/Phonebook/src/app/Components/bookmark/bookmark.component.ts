import { Component, OnInit } from '@angular/core';
import { contact } from 'src/app/models/contact';
import { ContactService } from 'src/app/Services/contact.service';

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.css']
})
export class BookmarkComponent implements OnInit {
  contacts:contact[];
  constructor(private contactService:ContactService) { }

  ngOnInit() {
    this.contactService.getAllBookmarked().subscribe((Response)=>{
        this.contacts = Response;
    });
  }

}
