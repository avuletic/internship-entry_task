import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { contact } from 'src/app/models/contact';
import { ContactService } from 'src/app/Services/contact.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  contacts:contact[];
  constructor(private contactService:ContactService,private route:ActivatedRoute) { 
  }

  ngOnInit() {
    switch(this.route.snapshot.url[0].path){
      case "name":{
          this.contactService.getContactsByName(this.route.snapshot.url[1].path).subscribe((Response)=>{
            this.contacts = Response;
          })
        break;
      }
      case "surname":{
        this.contactService.getContactsBySurname(this.route.snapshot.url[1].path).subscribe((Response)=>{
          this.contacts = Response;
        })
        break;
      }
      case "tag":{
        this.contactService.getContactsByTag(this.route.snapshot.url[1].path).subscribe((Response)=>{
          this.contacts = Response;
        })
        break;
      }
    }
  }

}
