import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';

import { ContactService } from '../../Services/contact.service'
import { contact } from '../../models/contact';
import { tag } from 'src/app/models/tag';
import { mail } from 'src/app/models/mail';
import { phoneNumber } from 'src/app/models/phonenumber';
@Component({
  selector: 'app-editcontact',
  templateUrl: './editcontact.component.html',
  styleUrls: ['./editcontact.component.css']
})
export class EditcontactComponent implements OnInit {
  contact:contact;
  editContactForm: FormGroup;
  constructor(private formBuilder:FormBuilder,private cntService:ContactService,private router:Router,private route:ActivatedRoute) {}
  ngOnInit():void{
    this.editContactForm = new FormGroup({
      contactId: new FormControl,
      name: new FormControl,
      surname: new FormControl,
      address: new FormControl,
      nickname: new FormControl,
      tags: this.formBuilder.array([]),
      mails: this.formBuilder.array([]),
      phoneNumbers: this.formBuilder.array([])
    });
    this.cntService.getContact(Number(this.route.snapshot.paramMap.get('id'))).subscribe((Response)=>{
      this.contact = Response;
      this.initForm();
    });

  }
  initForm(){
    this.editContactForm = this.formBuilder.group({
      contactId: this.contact.contactId,
      name:this.contact.name,
      surname:this.contact.surname,
      address:this.contact.address,
      nickname:this.contact.nickname,
      tags: this.formBuilder.array([]),
      mails: this.formBuilder.array([]),
      phoneNumbers: this.formBuilder.array([])
    });
    this.contact.tags.forEach((tag) =>{
      this.initTag(tag);
    });
    this.contact.mails.forEach((mail)=>{
      this.initMail(mail);
    });
    this.contact.phoneNumbers.forEach((phNumber)=>{
      this.initPhoneNumber(phNumber);
    });
  }
  get tags(){
    return this.editContactForm.get('tags') as FormArray;
  }
  initTag(tagToInit:tag){
    return this.tags.push(this.formBuilder.group({
      tagId: tagToInit.tagId,
      group: tagToInit.group
    }))
  }
  addTag(){
    this.tags.push(this.formBuilder.group({
      tagId: -1,
      group: ''
    }));
  }
  deleteTag(index:number){
    this.tags.removeAt(index);
  }
  get mails(){
    return this.editContactForm.get('mails') as FormArray;
  }
  initMail(mailToInit:mail){
    this.mails.push(this.formBuilder.group({
      mailId: mailToInit.mailId,
      email: mailToInit.email
    }))
  }
  addMail(){
    this.mails.push(this.formBuilder.group({
      mailId:-1,
      email: ''
    }));
  }
  deleteMail(index:number){
    this.mails.removeAt(index);
  }
  get phoneNumbers(){
    return this.editContactForm.get('phoneNumbers') as FormArray;
  }
  initPhoneNumber(phoneNumberToInit:phoneNumber){
    this.phoneNumbers.push(this.formBuilder.group({
      phoneNumberId: phoneNumberToInit.phoneNumberId,
      number: phoneNumberToInit.number
    }));
  }
  addPhoneNumber(){
    this.phoneNumbers.push(this.formBuilder.group({
      phoneNumberId:-1,
      number: ''
    }));
  }
  deletePhoneNumber(index:number){
    this.phoneNumbers.removeAt(index);
  }
  onSubmit(){
    let stagingContact = new contact(this.editContactForm.value);
    this.cntService.putContact(stagingContact).subscribe(()=>{
      this.router.navigateByUrl('/search');
    })
  }
  onClose(){
    this.router.navigateByUrl('/search');
  }

}
