import { Component, OnInit,Input } from '@angular/core';
import { tag } from 'src/app/models/tag';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.css']
})
export class TagComponent implements OnInit {
  @Input() tag:tag;
  constructor() { }

  ngOnInit() {
  }

}
