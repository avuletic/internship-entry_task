import { Component, OnInit,Input } from '@angular/core';
import { Router } from '@angular/router';

import { contact } from '../../models/contact';
import { ContactService } from 'src/app/Services/contact.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  @Input() cnt: contact;
  deleted:boolean = false;
  hiddenNumbers:boolean = true;
  hiddenMails:boolean = true;
  hiddenContact:boolean = true;
  constructor(private router:Router, private contactService:ContactService) { }
  ngOnInit() {
  }
  editContact():void{
    this.router.navigateByUrl(`edit/${this.cnt.contactId}`);
  }
  deleteContact():void{
    this.contactService.deleteContact(this.cnt).subscribe(() =>{
      this.deleted = true;
    });
  }
  toggleBookmark():void{
    this.contactService.toggleBookmark(this.cnt).subscribe((Response)=>{
      this.cnt = Response;
    });
  }
}
