import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';

import { ContactService } from '../../Services/contact.service'
import { contact } from '../../models/contact';
@Component({
  selector: 'app-addcontact',
  templateUrl: './addcontact.component.html',
  styleUrls: ['./addcontact.component.css']
})
export class AddcontactComponent implements OnInit{
  addContactForm: FormGroup;
  constructor(private formBuilder:FormBuilder,private cntService:ContactService,private router:Router) { 
  }
  ngOnInit():void{
    this.addContactForm = this.formBuilder.group({
      Name:[],
      Surname:[],
      Address:[],
      Nickname:[],
      Tags: this.formBuilder.array([this.formBuilder.group({Group: ''})]),
      Mails: this.formBuilder.array([this.formBuilder.group({Email: ''})]),
      phoneNumbers: this.formBuilder.array([this.formBuilder.group({Number: ''})])
    });
  }
  get tags(){
    return this.addContactForm.get('Tags') as FormArray;
  }
  addTag(){
    this.tags.push(this.formBuilder.group({Group:''}));
  }
  deleteTag(index:number){
    this.tags.removeAt(index);
  }
  get mails(){
    return this.addContactForm.get('Mails') as FormArray;
  }
  addMail(){
    this.mails.push(this.formBuilder.group({Email:''}));
  }
  deleteMail(index:number){
    this.mails.removeAt(index);
  }
  get phoneNumbers(){
    return this.addContactForm.get('phoneNumbers') as FormArray;
  }
  addPhoneNumber(){
    this.phoneNumbers.push(this.formBuilder.group({Number:''}));
  }
  deletePhoneNumber(index:number){
    this.phoneNumbers.removeAt(index);
  }
  onSubmit(){
    let tmpContact = new contact(this.addContactForm.value);
    this.cntService.postContact(tmpContact).subscribe((Response)=>{
      if(Response.contactId !== 0)
        this.router.navigateByUrl('/search');
    });

  }
}
