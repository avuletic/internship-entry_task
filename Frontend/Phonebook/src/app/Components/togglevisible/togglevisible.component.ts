import { Component, OnInit,Output,Input,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-togglevisible',
  templateUrl: './togglevisible.component.html',
  styleUrls: ['./togglevisible.component.css']
})
export class TogglevisibleComponent implements OnInit {
  @Input() currentState:boolean;
  @Output() public stateChanged = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  clicked(){
    this.stateChanged.emit(!this.currentState);
  }
}
