import { Component, OnInit } from '@angular/core';
import { contact } from 'src/app/models/contact';
import { ContactService } from 'src/app/Services/contact.service';  
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  contacts:contact[];
  constructor(private contactService:ContactService) { }

  ngOnInit() {
    this.contactService.getContacts().subscribe((Response)=>{
      this.contacts = Response;
    });
  }

}
