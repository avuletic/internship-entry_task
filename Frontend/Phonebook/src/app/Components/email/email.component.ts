import { Component, OnInit,Input } from '@angular/core';
import { mail } from 'src/app/models/mail';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {
  @Input() email:mail;
  constructor() { }

  ngOnInit() {
  }

}
