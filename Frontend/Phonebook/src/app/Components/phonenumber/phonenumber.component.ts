import { Component, OnInit,Input } from '@angular/core';
import { phoneNumber } from 'src/app/models/phonenumber';

@Component({
  selector: 'app-phonenumber',
  templateUrl: './phonenumber.component.html',
  styleUrls: ['./phonenumber.component.css']
})
export class PhonenumberComponent implements OnInit {
  @Input() number:phoneNumber;
  constructor() { }

  ngOnInit() {
  }

}
