import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactComponent } from './Components/contact/contact.component';
import { PhonenumberComponent } from './Components/phonenumber/phonenumber.component';
import { EmailComponent } from './Components/email/email.component';
import { TagComponent } from './Components/tag/tag.component';
import { TogglevisibleComponent } from './Components/togglevisible/togglevisible.component';
import { BookmarkComponent } from './Components/bookmark/bookmark.component';
import { AddcontactComponent } from './Components/addcontact/addcontact.component';
import { EditcontactComponent } from './Components/editcontact/editcontact.component';
import { HomeComponent } from './Components/home/home.component';
import { SearchComponent } from './Components/search/search.component';

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    PhonenumberComponent,
    EmailComponent,
    TagComponent,
    TogglevisibleComponent,
    BookmarkComponent,
    AddcontactComponent,
    EditcontactComponent,
    HomeComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
