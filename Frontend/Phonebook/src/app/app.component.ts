import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router, NavigationEnd } from '@angular/router';
import { routerNgProbeToken } from '@angular/router/src/router_module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  searchOptions = [
    {name:"name"},
    {name:"surname"},
    {name:"tag"}
  ];
  optionsToggle:boolean = true;
  title = 'Phonebook';
  constructor(private router:Router){}
  toggleOption(){
    this.optionsToggle = !this.optionsToggle;
  }
  onSubmit(searchForm:NgForm){
      this.router.routeReuseStrategy.shouldReuseRoute = function(){
        return false;
    };

    this.router.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd) {
            this.router.navigated = false;
            window.scrollTo(0, 0);
        }
    });
    switch(searchForm.value.searchOption){
      case "name":{
        this.router
        this.router.navigateByUrl(`/name/${searchForm.value.searchParam}`);
        break;
      }
      case "surname":{
        this.router.navigateByUrl(`/surname/${searchForm.value.searchParam}`);
        break;
      }
      case "tag":{
        this.router.navigateByUrl(`/tag/${searchForm.value.searchParam}`);
        break;
      }
    }
  }
}
