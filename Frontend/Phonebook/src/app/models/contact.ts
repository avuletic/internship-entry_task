import { tag } from './tag';
import { mail } from './mail';
import { phoneNumber } from './phonenumber';

export class contact{
    contactId: number;
    name: string;
    surname: string;
    address: string;
    nickname: string;
    tags: tag[];
    mails: mail[];
    phoneNumbers: phoneNumber[];
    isBookmarked: boolean;

    public constructor(init?: Partial<contact>) {
        Object.assign(this,init);
    }
}