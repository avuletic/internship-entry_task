import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { contact } from '../models/contact';
import { mail } from '../models/mail';
import { phoneNumber } from '../models/phonenumber';
import { tag } from '../models/tag';

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private backendEndpoint:String = 'https://localhost:44343/api/contact';
  private getByName:String = '/getbyname';
  private getBySurname:String = '/getbysurname';
  private postToggleBookmark:String = '/posttogglebookmark';
  private getByTag:String = '/getbytag';
  private getBookmarked:String = '/getbookmarked';
  private get:String = '/get';
  private put:String = '/put';
  private post:String = '/post';
  private delete:String = '/delete';

  constructor(private HTTP:HttpClient) { 

  }
  getContacts():Observable<contact[]>{
    return this.HTTP.get<contact[]>(`${this.backendEndpoint}${this.get}`);
  }
  getContact(id:number):Observable<contact>{
    return this.HTTP.get<contact>(`${this.backendEndpoint}${this.get}/${id}`);
  }
  getContactsByName(name:string):Observable<contact[]>{
    return this.HTTP.get<contact[]>(`${this.backendEndpoint}${this.getByName}/${name}`);
  }
  getContactsBySurname(surname:string):Observable<contact[]>{
    return this.HTTP.get<[contact]>(`${this.backendEndpoint}${this.getBySurname}/${surname}`);
  }
  getContactsByTag(tag:string):Observable<contact[]>{
    return this.HTTP.get<contact[]>(`${this.backendEndpoint}${this.getByTag}/${tag}`);
  }
  getAllBookmarked():Observable<contact[]>{
    return this.HTTP.get<contact[]>(`${this.backendEndpoint}${this.getBookmarked}`);
  }
  toggleBookmark(contactToToggle:contact):Observable<contact>{
    return this.HTTP.post<contact>(`${this.backendEndpoint}${this.postToggleBookmark}`, contactToToggle)
  }
  postContact(demoContact:contact):Observable<contact>{
    return this.HTTP.post<contact>(`${this.backendEndpoint}${this.post}`,demoContact);
  }
  putContact(contact:contact):Observable<contact>{
    console.log(contact)
    return this.HTTP.put<contact>(`${this.backendEndpoint}${this.put}/${String(contact.contactId)}`,contact);
  }
  deleteContact(removeContact:contact):Observable<contact>{
    return this.HTTP.delete<contact>(`${this.backendEndpoint}${this.delete}/${removeContact.contactId}`);
  }
}
