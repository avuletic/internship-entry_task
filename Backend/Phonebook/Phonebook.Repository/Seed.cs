﻿using System.Linq;
using Phonebook.Models;
using Phonebook.Models.Entities;

namespace Phonebook.Repository
{
    public class Seed
    {
        private readonly Context _context;
        public Seed()
        {
            _context = new Context();
            if (_context.Contacts.Any()) return;
            _context.Contacts.Add(new Contact("Vula","Ante", "Vuletić", "Split"));
            _context.Contacts.Add(new Contact("Duje","Dujam", "Krstulović", "Split"));
            _context.Contacts.Add(new Contact("Domminik HR","Dominik", "Borović", "Zagreb"));
            _context.SaveChanges();
            _context.ContactPhoneNumbers.Add(new ContactPhoneNumber("12345",
                _context.Contacts.FirstOrDefault(cnt => cnt.Name == "Ante")));
            _context.ContactPhoneNumbers.Add(new ContactPhoneNumber("54321",
                _context.Contacts.FirstOrDefault(cnt => cnt.Name == "Dujam")));
            _context.ContactPhoneNumbers.Add(new ContactPhoneNumber("22222",
                _context.Contacts.FirstOrDefault(cnt => cnt.Name == "Dominik")));
            _context.SaveChanges();
            _context.ContactMails.Add(new ContactMail("antevuletic1995@gmail.com",
                _context.Contacts.FirstOrDefault(cnt => cnt.Name == "Ante")));
            _context.ContactMails.Add(new ContactMail("dujam.krst@gmail.com",
                _context.Contacts.FirstOrDefault(cnt => cnt.Name == "Dujam")));
            _context.ContactMails.Add(new ContactMail("nekiMail@gmail.com",
                _context.Contacts.FirstOrDefault(cnt => cnt.Name == "Ante")));
            _context.SaveChanges();
            _context.Tags.Add(new Tag("Work"));
            _context.SaveChanges();
            _context.ContactTags.Add(new ContactTag(_context.Contacts.FirstOrDefault(cnt => cnt.Name == "Ante"),
            _context.Tags.FirstOrDefault(tg => tg.Group == "Work")));
            _context.ContactTags.Add(new ContactTag(_context.Contacts.FirstOrDefault(cnt => cnt.Name == "Dujam"),
                _context.Tags.FirstOrDefault(tg => tg.Group == "Work")));
            _context.ContactTags.Add(new ContactTag(_context.Contacts.FirstOrDefault(cnt => cnt.Name == "Dominik"),
                _context.Tags.FirstOrDefault(tg => tg.Group == "Work")));
            _context.SaveChanges();
        }
    }
}
