﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Phonebook.Models;
using Phonebook.Models.Entities;

namespace Phonebook.Repository
{
    public class TagRepository
    {
        private readonly Context _context;

        public TagRepository()
        {
            _context = new Context();
        }

        public Tag GetTag(int tagId)
        {
            return _context.Tags.Find(tagId);
        }
        public async Task<Tag> GetTagAsync(int tagId)
        {
            return await _context.Tags.FindAsync(tagId);
        }

        public IQueryable<Tag> GetAllTags()
        {
            return _context.Tags;
        }
        public async Task<IQueryable<Tag>> GetAllTagsAsync()
        {
            return await Task.Run(() => _context.Tags);
        }
        public IQueryable<Tag> GetAllTagsByContact(int contactId)
        {
            return _context.Tags.Where(tg => tg.ContactTags.Any(cntTag => cntTag.ContactId == contactId));
        }
        public async Task<IQueryable<Tag>> GetAllTagsByContactAsync(int contactId)
        {
            return await Task.Run(()=> _context.Tags.Where(tg => tg.ContactTags.Any(cntTag => cntTag.ContactId == contactId)));
        }
        public Tag AddTag(string group)
        {
            var hasTag = _context.Tags.FirstOrDefault(tg =>
                string.Equals(tg.Group, group, StringComparison.CurrentCultureIgnoreCase));
            if (hasTag != null)
                return hasTag;
            var newTag = new Tag(group);
            _context.Tags.Add(newTag);
            _context.SaveChanges();
            return newTag;
        }
        public async Task<Tag> AddTagAsync(string group)
        {
            var hasTag = await _context.Tags.FirstOrDefaultAsync(tg =>
                string.Equals(tg.Group, group, StringComparison.CurrentCultureIgnoreCase));
            if (hasTag != null)
                return hasTag;
            var newTag = new Tag(group);
            await _context.Tags.AddAsync(newTag);
            await _context.SaveChangesAsync();
            return newTag;
        }

        public void AddTagToContact(int contactId, Tag tag)
        {
            var relationContactTag = _context.ContactTags.FirstOrDefault(
                cntTag => cntTag.ContactId == contactId && cntTag.Tag.TagId == tag.TagId);
            if (relationContactTag != null)
                return;
            _context.ContactTags.Add(new ContactTag(_context.Contacts.Find(contactId), tag));
            _context.SaveChanges();
        }

        public async Task AddTagToContactAsync(int contactId, Tag tag)
        {
            var relationContactTag = await Task.Run(() =>_context.ContactTags.FirstOrDefault(
                cntTag => cntTag.ContactId == contactId && cntTag.Tag.TagId == tag.TagId));
            if (relationContactTag != null)
                return;
            await _context.ContactTags.AddAsync(new ContactTag(await _context.Contacts.FindAsync(contactId), tag));
            await _context.SaveChangesAsync();
        }

        public void RemoveTagFromContact(int contactId, int tagId)
        {
            var foundTag = GetTag(tagId);
            if(foundTag == null) throw new Exception("Fatal sync error fronted and backend");
            var contactTags = _context.ContactTags.Where(cntTag => cntTag.Tag == foundTag);
            _context.RemoveRange(contactTags);
            _context.SaveChanges();
            contactTags = _context.ContactTags.Where(cntTag => cntTag.Tag == foundTag);
            if (contactTags.Any()) return;
            _context.Tags.Remove(foundTag);
            _context.SaveChanges();
        }
        public async Task RemoveTagFromContactAsync(int contactId, int tagId)
        {
            var foundTag = await GetTagAsync(tagId);
            if (foundTag == null) throw new Exception("Fatal sync error fronted and backend");
            var contactTags = await Task.Run( () =>_context.ContactTags.Where(cntTag => cntTag.Tag == foundTag));
            _context.RemoveRange(contactTags);
            await _context.SaveChangesAsync();
            contactTags = await Task.Run(() => _context.ContactTags.Where(cntTag => cntTag.Tag == foundTag));
            if (await contactTags.AnyAsync()) return;
            _context.Tags.Remove(foundTag);
            await _context.SaveChangesAsync();
        }
    }
}
