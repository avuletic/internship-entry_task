﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Phonebook.Models;
using Phonebook.Models.Entities;

namespace Phonebook.Repository
{
    public class ContactRepository
    {
        private readonly Context _context;

        public ContactRepository()
        {
            _context = new Context();
        }

        public Contact GetContact(int contactId)
        {
            return _context.Contacts.Find(contactId);
        }

        public async Task<Contact> GetContactAsync(int contactId)
        {
            return await Task.Run( () => _context.Contacts.Find(contactId));
        }
        public IQueryable<Contact> GetAllContacts()
        {
            return _context.Contacts;
        }
        public async Task<IQueryable<Contact>> GetAllContactsAsync()
        {
            return await Task.Run(() => _context.Contacts);
        }
        public IQueryable<Contact> GetAllBookmarked()
        {
            return _context.Contacts.Where(cnt => cnt.IsBookmarked);
        }
        public async Task<IQueryable<Contact>> GetAllBookmarkedAsync()
        {
            return await Task.Run(() => _context.Contacts.Where(cnt => cnt.IsBookmarked));
        }
        public IQueryable<Contact> GetByName(string name)
        {
            return _context.Contacts.Where(cnt => cnt.Name.ToLower().Contains(name.ToLower()));
        }

        public async Task<IQueryable<Contact>> GetByNameAsync(string name)
        {
            return await Task.Run(() => _context.Contacts.Where(cnt => cnt.Name.ToLower().Contains(name.ToLower())));
        }
        public IQueryable<Contact> GetBySurname(string surname)
        {
            return _context.Contacts.Where(cnt => cnt.Surname.ToLower().Contains(surname.ToLower()));
        }
        public async Task<IQueryable<Contact>> GetBySurnameAsync(string surname)
        {
            return await Task.Run(() => _context.Contacts.Where(cnt => cnt.Surname.ToLower().Contains(surname.ToLower())));
        }

        public IQueryable<Contact> GetByTag(string tag)
        {
            return _context.Contacts.Where(cnt => cnt.ContactTags.Any(cntTag => cntTag.Tag.Group.ToLower().Contains(tag.ToLower())));
        }
        public async Task<IQueryable<Contact>> GetByTagAsync(string tag)
        {
            return await Task.Run(() => _context.Contacts.Where(cnt => cnt.ContactTags.Any(cntTag => cntTag.Tag.Group.ToLower().Contains(tag.ToLower()))));
        }
        public Contact AddContact(string name, string surname, string nickname, string address ="")
        {
            if (_context.Contacts.Any(cont =>
                string.Equals(cont.Nickname, nickname, StringComparison.CurrentCultureIgnoreCase)))
                throw new Exception("Contact Nickname already exists");
            var newContact = new Contact(nickname, name, surname, address);
            _context.Contacts.Add(newContact);
            _context.SaveChanges();
            return newContact;
        }
        public async Task<Contact> AddContactAsync(string name, string surname, string nickname, string address = "")
        {
            if (await _context.Contacts.AnyAsync(cont =>
                string.Equals(cont.Nickname, nickname, StringComparison.CurrentCultureIgnoreCase)))
                throw new Exception("Contact Nickname already exists");
            var newContact = new Contact(nickname, name, surname, address);
            await _context.Contacts.AddAsync(newContact);
            await _context.SaveChangesAsync();
            return newContact;
        }

        public void ToggleBookmark(int personId)
        {
            var contact = _context.Contacts.Find(personId);
            if(contact == null) throw new Exception("Contact not exist");
            contact.IsBookmarked = !contact.IsBookmarked;
            _context.SaveChanges();
        }
        public async Task ToggleBookmarkAsync(int personId)
        {
            var contact = await _context.Contacts.FindAsync(personId);
            if (contact == null) throw new Exception("Contact not exist");
            contact.IsBookmarked = !contact.IsBookmarked;
            await _context.SaveChangesAsync();
        }

        public void RemoveContact(int contactId)
        {
            var contactFound = GetContact(contactId);
            if (contactFound == null)
                throw new Exception("Contact not found");
            _context.ContactMails.RemoveRange(_context.ContactMails.Where(ml => ml.Contact == contactFound));
            _context.ContactPhoneNumbers.RemoveRange(_context.ContactPhoneNumbers.Where(ph => ph.Contact == contactFound));
            _context.ContactTags.RemoveRange(_context.ContactTags.Where(cntTag => cntTag.Contact == contactFound));
            _context.Contacts.Remove(contactFound);
            _context.SaveChanges();
        }
        public async Task RemoveContactAsync(int contactId)
        {
            var contactFound = await GetContactAsync(contactId);
            if (contactFound == null)
                throw new Exception("Contact not found");
            _context.ContactMails.RemoveRange(_context.ContactMails.Where(ml => ml.Contact == contactFound));
            _context.ContactPhoneNumbers.RemoveRange(_context.ContactPhoneNumbers.Where(ph => ph.Contact == contactFound));
            _context.ContactTags.RemoveRange(_context.ContactTags.Where(cntTag => cntTag.Contact == contactFound));
            _context.Contacts.Remove(contactFound);
            await _context.SaveChangesAsync();
        }

        public Contact EditContact(int contactId, string name, string surname, string nickname, string address)
        {
            var foundContact = GetContact(contactId);
            if (foundContact == null) throw new Exception("Contact not exist");
            foundContact.Name = name;
            foundContact.Surname = surname;
            foundContact.Address = address;
            foundContact.Nickname = nickname;
            _context.SaveChanges();
            return foundContact;
        }
        public async Task<Contact> EditContactAsync(int contactId, string name, string surname, string nickname, string address)
        {
            var foundContact = await GetContactAsync(contactId);
            if (foundContact == null) throw new Exception("Contact not exist");
            foundContact.Name = name;
            foundContact.Surname = surname;
            foundContact.Address = address;
            foundContact.Nickname = nickname;
            await _context.SaveChangesAsync();
            return foundContact;
        }
    }
}
