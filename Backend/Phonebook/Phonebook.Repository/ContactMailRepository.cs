﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Phonebook.Models;
using Phonebook.Models.Entities;

namespace Phonebook.Repository
{
    public class ContactMailRepository
    {
        private readonly Context _context;

        public ContactMailRepository()
        {
            _context = new Context();
        }

        public ContactMail GetContactMail(int contactMailId)
        {
            return _context.ContactMails.Find(contactMailId);
        }
        public async Task<ContactMail> GetContactMailAsync(int contactMailId)
        {
            return await Task.Run(()=> _context.ContactMails.Find(contactMailId));
        }

        public IQueryable<ContactMail> GetAllMailsByContact(int personId)
        {
            return _context.ContactMails.Where(cntMail => cntMail.Contact.PersonId == personId);
        }
        public async Task<IQueryable<ContactMail>> GetAllMailsByContactAsync(int personId)
        {
            return await Task.Run(() => _context.ContactMails.Where(cntMail => cntMail.Contact.PersonId == personId));
        }
        public void RemoveContactMail(int contactMailId)
        {
            var contactMail = GetContactMail(contactMailId);
            if (contactMail == null) throw new Exception("Failed to find mail");
            _context.ContactMails.Remove(contactMail);
            _context.SaveChanges();
        }
        public async Task RemoveContactMailAsync(int contactMailId)
        {
            var contactMail = await GetContactMailAsync(contactMailId);
            if (contactMail == null) throw new Exception("Failed to find mail");
            _context.ContactMails.Remove(contactMail);
            await _context.SaveChangesAsync();
        }

        public ContactMail AddContactMail(string email, int contactId)
        {
            if (_context.ContactMails.Any(m =>
                string.Equals(m.Email, email, StringComparison.CurrentCultureIgnoreCase)))
                throw new Exception("Mail already exist");
            var newContactMail = new ContactMail(email, _context.Contacts.Find(contactId));
            _context.ContactMails.Add(newContactMail);
            _context.SaveChanges();
            return newContactMail;
        }
        public async Task<ContactMail> AddContactMailAsync(string email, int contactId)
        {
            if (await _context.ContactMails.AnyAsync(m =>
                string.Equals(m.Email, email, StringComparison.CurrentCultureIgnoreCase)))
                throw new Exception("Mail already exist");
            var newContactMail = new ContactMail(email, _context.Contacts.Find(contactId));
            await _context.ContactMails.AddAsync(newContactMail);
            await _context.SaveChangesAsync();
            return newContactMail;
        }
        public void EditContactMail(int mailId, string newEmail)
        {
            var contactMail = _context.ContactMails.Find(mailId);
            if (contactMail == null) throw new Exception("Mail not exist");
            contactMail.Email = newEmail;
            _context.SaveChanges();
        }
        public async Task EditContactMailAsync(int mailId, string newEmail)
        {
            var contactMail = await _context.ContactMails.FindAsync(mailId);
            if (contactMail == null) throw new Exception("Mail not exist");
            contactMail.Email = newEmail;
            await _context.SaveChangesAsync();
        }
    }
}
