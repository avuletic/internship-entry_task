﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Phonebook.Models;
using Phonebook.Models.Entities;

namespace Phonebook.Repository
{
    public class ContactPhoneNumberRepository
    {
        private readonly Context _context;

        public ContactPhoneNumberRepository()
        {
            _context = new Context();
        }

        public ContactPhoneNumber GetPhoneNumber(int contactPhoneNumberId)
        {
            return _context.ContactPhoneNumbers.Find(contactPhoneNumberId);
        }
        public async Task<ContactPhoneNumber> GetPhoneNumberAsync(int contactPhoneNumberId)
        {
            return await _context.ContactPhoneNumbers.FindAsync(contactPhoneNumberId);
        }

        public IQueryable<ContactPhoneNumber> GetPhoneNumbersByContact(int personId)
        {
            return _context.ContactPhoneNumbers.Where(cntPhoneNumber => cntPhoneNumber.Contact.PersonId == personId);
        }
        public async Task<IQueryable<ContactPhoneNumber>> GetPhoneNumbersByContactAsync(int personId)
        {
            return await Task.Run(()=> _context.ContactPhoneNumbers.Where(cntPhoneNumber => cntPhoneNumber.Contact.PersonId == personId));
        }

        public void RemovePhoneNumber(int contactPhoneNumberId)
        {
            var foundPhoneNumber = GetPhoneNumber(contactPhoneNumberId);
            if (foundPhoneNumber == null) throw new Exception("Phone number not exist");
            _context.ContactPhoneNumbers.Remove(foundPhoneNumber);
            _context.SaveChanges();
        }
        public async Task RemovePhoneNumberAsync(int contactPhoneNumberId)
        {
            var foundPhoneNumber = await GetPhoneNumberAsync(contactPhoneNumberId);
            if (foundPhoneNumber == null) throw new Exception("Phone number not exist");
            _context.ContactPhoneNumbers.Remove(foundPhoneNumber);
            await _context.SaveChangesAsync();
        }

        public ContactPhoneNumber AddPhoneNumber(int contactId, string phoneNumber)
        {
            if (_context.ContactPhoneNumbers.Any(phNumber =>
                string.Equals(phoneNumber, phNumber.PhoneNumber, StringComparison.CurrentCultureIgnoreCase)))
                throw new Exception("Duplicate phone number");
            var newPhoneNumber = new ContactPhoneNumber(phoneNumber, _context.Contacts.Find(contactId));
            _context.ContactPhoneNumbers.Add(newPhoneNumber);
            _context.SaveChanges();
            return newPhoneNumber;
        }
        public async Task<ContactPhoneNumber> AddPhoneNumberAsync(int contactId, string phoneNumber)
        {
            if (await _context.ContactPhoneNumbers.AnyAsync(phNumber =>
                string.Equals(phoneNumber, phNumber.PhoneNumber, StringComparison.CurrentCultureIgnoreCase)))
                throw new Exception("Duplicate phone number");
            var newPhoneNumber = new ContactPhoneNumber(phoneNumber, _context.Contacts.Find(contactId));
            await _context.ContactPhoneNumbers.AddAsync(newPhoneNumber);
            await _context.SaveChangesAsync();
            return newPhoneNumber;
        }

        public void EditPhoneNumber(int phoneNumberId, string newPhoneNumber)
        {
            var foundPhoneNumber = _context.ContactPhoneNumbers.Find(phoneNumberId);
            if (foundPhoneNumber == null) throw new Exception("Number not exist.");
            foundPhoneNumber.PhoneNumber = newPhoneNumber;
            _context.SaveChanges();
        }
        public async Task EditPhoneNumberAsync(int phoneNumberId, string newPhoneNumber)
        {
            var foundPhoneNumber = await _context.ContactPhoneNumbers.FindAsync(phoneNumberId);
            if (foundPhoneNumber == null) throw new Exception("Number not exist.");
            foundPhoneNumber.PhoneNumber = newPhoneNumber;
            await _context.SaveChangesAsync();
        }
    }
}
