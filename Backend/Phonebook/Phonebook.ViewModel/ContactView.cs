﻿using System.Collections.Generic;

namespace Phonebook.ViewModel
{
    public class ContactView
    {
        public int ContactId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public string Nickname { get; set; }
        public bool IsBookmarked { get; set; }
        public ICollection<TagView> Tags { get; set; }
        public ICollection<PhoneNumberView> PhoneNumbers { get; set; }
        public ICollection<MailView> Mails { get; set; }

        public ContactView(int contactId, string name, string surname, string address, string nickname, ICollection<TagView> tags, ICollection<PhoneNumberView> phoneNumbers, ICollection<MailView> mails, bool isBookmarked)
        {
            ContactId = contactId;
            Name = name;
            Surname = surname;
            Address = address;
            Nickname = nickname;
            Tags = tags;
            PhoneNumbers = phoneNumbers;
            Mails = mails;
            IsBookmarked = isBookmarked;
        }
    }
}
