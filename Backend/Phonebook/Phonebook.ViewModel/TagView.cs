﻿namespace Phonebook.ViewModel
{
    public class TagView
    {
        public int TagId { get; set; }
        public string Group { get; set; }

        public TagView(int tagId, string group)
        {
            TagId = tagId;
            Group = group;
        }
    }
}
