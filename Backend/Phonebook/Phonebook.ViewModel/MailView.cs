﻿namespace Phonebook.ViewModel
{
    public class MailView
    {
        public int MailId { get; set; }
        public string Email { get; set; }

        public MailView(int mailId, string email)
        {
            MailId = mailId;
            Email = email;
        }
    }
}
