﻿namespace Phonebook.ViewModel
{
    public class PhoneNumberView
    {
        public int PhoneNumberId { get; set; }
        public string Number { get; set; }

        public PhoneNumberView(int phoneNumberId, string number)
        {
            PhoneNumberId = phoneNumberId;
            Number = number;
        }
    }
}
