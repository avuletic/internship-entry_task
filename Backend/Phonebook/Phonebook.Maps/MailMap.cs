﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phonebook.Repository;
using Phonebook.ViewModel;

namespace Phonebook.Maps
{
    public class MailMap
    {
        private readonly ContactMailRepository _contactMailRepository;

        public MailMap()
        {
            _contactMailRepository = new ContactMailRepository();
        }

        public ICollection<MailView> GetAllByContactId(int contactId)
        {
            var mailsOfContact = new List<MailView>();
            foreach (var mail in _contactMailRepository.GetAllMailsByContact(contactId))
            {
                mailsOfContact.Add(new MailView(mail.ContactMailId,mail.Email));
            }

            return mailsOfContact;
        }
        public async Task<ICollection<MailView>> GetAllByContactIdAsync(int contactId)
        {
            var mailsOfContact = new List<MailView>();
            foreach (var mail in await _contactMailRepository.GetAllMailsByContactAsync(contactId))
            {
                mailsOfContact.Add(new MailView(mail.ContactMailId, mail.Email));
            }

            return mailsOfContact;
        }

        public void PostMail(int contactId, MailView mail)
        {
            var contactMail = _contactMailRepository.AddContactMail(mail.Email, contactId);
            mail.MailId = contactMail.ContactMailId;
        }

        public async Task PostMailAsync(int contactId, MailView mail)
        {
            var contactMail = await _contactMailRepository.AddContactMailAsync(mail.Email, contactId);
            mail.MailId = contactMail.ContactMailId;
        }

        public void PutRange(int contactId, ICollection<MailView> mails)
        {
            var allMailsByContact = GetAllByContactId(contactId);
            foreach (var mailView in allMailsByContact)
            {
                var currentMail = mails.FirstOrDefault(newMail => newMail.MailId == mailView.MailId);
                if (currentMail == null)
                {
                    DeleteMail(mailView.MailId);
                }
                else
                {
                    PutMail(currentMail);
                }
            }

            foreach (var mailView in mails.Where(ml => ml.MailId == -1))
            {
               PostMail(contactId,mailView);
            }
        }
        public async Task PutRangeAsync(int contactId, ICollection<MailView> mails)
        {
            var allMailsByContact = await GetAllByContactIdAsync(contactId);
            foreach (var mailView in allMailsByContact)
            {
                var currentMail = mails.FirstOrDefault(newMail => newMail.MailId == mailView.MailId);
                if (currentMail == null)
                {
                    await DeleteMailAsync(mailView.MailId);
                }
                else
                {
                    await PutMailAsync(currentMail);
                }
            }

            foreach (var mailView in mails.Where(ml => ml.MailId == -1))
            {
                await PostMailAsync(contactId, mailView);
            }
        }
        public void PutMail(MailView mail)
        {
            _contactMailRepository.EditContactMail(mail.MailId,mail.Email);
        }
        public async Task PutMailAsync(MailView mail)
        {
            await _contactMailRepository.EditContactMailAsync(mail.MailId, mail.Email);
        }
        public void DeleteMail(int mailId)
        {
            _contactMailRepository.RemoveContactMail(mailId);
        }
        public async Task DeleteMailAsync(int mailId)
        {
            await _contactMailRepository.RemoveContactMailAsync(mailId);
        }
    }
}
