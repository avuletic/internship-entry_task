﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Phonebook.Repository;
using Phonebook.ViewModel;

namespace Phonebook.Maps
{
    public class TagMap
    {
        private readonly TagRepository _tagRepository;

        public TagMap()
        {
            _tagRepository = new TagRepository();
        }

        public ICollection<TagView> GetTagsByContact(int contactId)
        {
            var tagsOfUser = new List<TagView>();
            foreach (var tag in _tagRepository.GetAllTagsByContact(contactId) )
            {
                tagsOfUser.Add(new TagView(tag.TagId,tag.Group));
            }
            return tagsOfUser;
        }
        public async Task<ICollection<TagView>> GetTagsByContactAsync(int contactId)
        {
            var tagsOfUser = new List<TagView>();
            foreach (var tag in await _tagRepository.GetAllTagsByContactAsync(contactId))
            {
                tagsOfUser.Add(new TagView(tag.TagId, tag.Group));
            }
            return tagsOfUser;
        }

        public void PostTagToContactRange(int contactId, ICollection<TagView> tags)
        {
            var allCurrentContactTags = GetTagsByContact(contactId);
            foreach (var allCurrentContactTag in allCurrentContactTags)
            {
                foreach (var tagView in tags)
                {
                    if (!string.Equals(allCurrentContactTag.Group, tagView.Group, StringComparison.CurrentCultureIgnoreCase))
                    {
                        PostTagToContact(contactId, tagView);
                    }
                }

                if (!tags.Any(newTag => string.Equals(allCurrentContactTag.Group, newTag.Group, StringComparison.CurrentCultureIgnoreCase)))
                    RemoveTagFromContact(contactId, allCurrentContactTag.TagId);
            }
        }
        public async Task PostTagToContactRangeAsync(int contactId, ICollection<TagView> tags)
        {
            var allCurrentContactTags = await GetTagsByContactAsync(contactId);
            foreach (var allCurrentContactTag in allCurrentContactTags)
            {            
                foreach (var tagView in tags)
                {
                    if(!string.Equals(allCurrentContactTag.Group, tagView.Group, StringComparison.CurrentCultureIgnoreCase))
                    {
                        await PostTagToContactAsync(contactId, tagView);
                    }
                }

                if (!tags.Any(newTag => string.Equals(allCurrentContactTag.Group, newTag.Group, StringComparison.CurrentCultureIgnoreCase)))
                    await RemoveTagFromContactAsync(contactId, allCurrentContactTag.TagId);
            }
        }
        public void PostTagToContact(int contactId,TagView tagView)
        {
            var tagModel = _tagRepository.AddTag(tagView.Group);
            _tagRepository.AddTagToContact(contactId,tagModel);
            tagView.TagId = tagModel.TagId;
        }
        public async Task PostTagToContactAsync(int contactId, TagView tagView)
        {
            var tagModel = await _tagRepository.AddTagAsync(tagView.Group);
            await _tagRepository.AddTagToContactAsync(contactId, tagModel);
            tagView.TagId = tagModel.TagId;
        }

        public void RemoveTagFromContact(int contactId, int tagViewId)
        {
            _tagRepository.RemoveTagFromContact(contactId,tagViewId);
        }
        public async Task RemoveTagFromContactAsync(int contactId, int tagViewId)
        {
            await _tagRepository.RemoveTagFromContactAsync(contactId, tagViewId);
        }
    }
}
