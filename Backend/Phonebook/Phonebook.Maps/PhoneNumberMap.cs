﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Phonebook.Repository;
using Phonebook.ViewModel;

namespace Phonebook.Maps
{
    public class PhoneNumberMap
    {
        private readonly ContactPhoneNumberRepository _contactPhoneNumberRepository;

        public PhoneNumberMap()
        {
            _contactPhoneNumberRepository = new ContactPhoneNumberRepository();
        }

        public ICollection<PhoneNumberView> GetAllPhoneNumbersByContact(int contactId)
        {
            var contactNumbers = new List<PhoneNumberView>();
            foreach (var number in _contactPhoneNumberRepository.GetPhoneNumbersByContact(contactId))
            {
                contactNumbers.Add(new PhoneNumberView(number.ContactPhoneNumberId,number.PhoneNumber));
            }

            return contactNumbers;
        }
        public async Task<ICollection<PhoneNumberView>> GetAllPhoneNumbersByContactAsync(int contactId)
        {
            var contactNumbers = new List<PhoneNumberView>();
            foreach (var number in await _contactPhoneNumberRepository.GetPhoneNumbersByContactAsync(contactId))
            {
                contactNumbers.Add(new PhoneNumberView(number.ContactPhoneNumberId, number.PhoneNumber));
            }

            return contactNumbers;
        }

        public void PostPhoneNumber(int contactId, PhoneNumberView phoneNumberView)
        {
            var phoneNumberModel = _contactPhoneNumberRepository.AddPhoneNumber(contactId, phoneNumberView.Number);
            phoneNumberView.PhoneNumberId = phoneNumberModel.ContactPhoneNumberId;
        }
        public async Task PostPhoneNumberAsync(int contactId, PhoneNumberView phoneNumberView)
        {
            var phoneNumberModel = await _contactPhoneNumberRepository.AddPhoneNumberAsync(contactId, phoneNumberView.Number);
            phoneNumberView.PhoneNumberId = phoneNumberModel.ContactPhoneNumberId;
        }

        public void PutRange(int contactId, ICollection<PhoneNumberView> phoneNumbers)
        {
            var allNumbersByContact = GetAllPhoneNumbersByContact(contactId);
            foreach (var phoneNumberView in allNumbersByContact)
            {
                var currentNumber = phoneNumbers.FirstOrDefault(newNumber =>
                    newNumber.PhoneNumberId == phoneNumberView.PhoneNumberId);
                if (currentNumber == null)
                {
                    DeletePhoneNumber(phoneNumberView.PhoneNumberId);
                }
                else
                {
                    PutPhoneNumber(currentNumber);
                }
            }

            foreach (var phoneNumberView in phoneNumbers.Where(phNumber => phNumber.PhoneNumberId == -1))
            {
                PostPhoneNumber(contactId,phoneNumberView);
            }
        }
        public async Task PutRangeAsync(int contactId, ICollection<PhoneNumberView> phoneNumbers)
        {
            var allNumbersByContact = await GetAllPhoneNumbersByContactAsync(contactId);
            foreach (var phoneNumberView in allNumbersByContact)
            {
                var currentNumber = phoneNumbers.FirstOrDefault(newNumber =>
                    newNumber.PhoneNumberId == phoneNumberView.PhoneNumberId);
                if (currentNumber == null)
                {
                    await DeletePhoneNumberAsync(phoneNumberView.PhoneNumberId);
                }
                else
                {
                    await PutPhoneNumberAsync(currentNumber);
                }
            }

            foreach (var phoneNumberView in phoneNumbers.Where(phNumber => phNumber.PhoneNumberId == -1))
            {
                await PostPhoneNumberAsync(contactId, phoneNumberView);
            }
        }
        public void PutPhoneNumber(PhoneNumberView phoneNumberView)
        {
            _contactPhoneNumberRepository.EditPhoneNumber(phoneNumberView.PhoneNumberId, phoneNumberView.Number);
        }
        public async Task PutPhoneNumberAsync(PhoneNumberView phoneNumberView)
        {
            await _contactPhoneNumberRepository.EditPhoneNumberAsync(phoneNumberView.PhoneNumberId, phoneNumberView.Number);
        }


        public void DeletePhoneNumber(int phoneNumberId)
        {
            _contactPhoneNumberRepository.RemovePhoneNumber(phoneNumberId);
        }
        public async Task DeletePhoneNumberAsync(int phoneNumberId)
        {
            await _contactPhoneNumberRepository.RemovePhoneNumberAsync(phoneNumberId);
        }
    }
}
