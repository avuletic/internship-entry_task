﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Phonebook.Models.Entities;
using Phonebook.Repository;
using Phonebook.ViewModel;

namespace Phonebook.Maps
{
    public class ContactMap
    {
        private readonly ContactRepository _contactRepository;
        private readonly MailMap _mailMap;
        private readonly TagMap _tagMap;
        private readonly PhoneNumberMap _phoneNumberMap;

        public ContactMap()
        {
            _contactRepository = new ContactRepository();
            _mailMap = new MailMap();
            _tagMap = new TagMap();
            _phoneNumberMap = new PhoneNumberMap();
        }

        private ICollection<ContactView> MapContactViews(IQueryable<Contact> contacts)
        {
             return contacts.Select(contact => 
                    new ContactView(
                    contact.PersonId, 
                    contact.Name, contact.Surname,
                    contact.Address, 
                    contact.Nickname, 
                    _tagMap.GetTagsByContact(contact.PersonId),
                    _phoneNumberMap.GetAllPhoneNumbersByContact(contact.PersonId), 
                    _mailMap.GetAllByContactId(contact.PersonId),
                    contact.IsBookmarked)).ToList();

        }
        public async Task<ICollection<ContactView>> GetAllAsync()
        {
            var contacts = await _contactRepository.GetAllContactsAsync();
            return MapContactViews(contacts);
        }
        public ICollection<ContactView> GetAllBookmarked()
        {
            return  MapContactViews(_contactRepository.GetAllBookmarked());
        }
        public async Task<ICollection<ContactView>> GetAllBookmarkedAsync()
        {
            var contacts = await _contactRepository.GetAllBookmarkedAsync();
            return MapContactViews(contacts);
        }
        public ICollection<ContactView> GetByName(string name)
        {
            var contacts = _contactRepository.GetByName(name);
            return MapContactViews(contacts);
        }
        public async Task<ICollection<ContactView>> GetByNameAsync(string name)
        {
            var contacts = await _contactRepository.GetByNameAsync(name);
            return MapContactViews(contacts);
        }
        public ICollection<ContactView> GetBySurname(string surname)
        {
            var contacts = _contactRepository.GetBySurname(surname);
            return MapContactViews(contacts);
        }
        public async Task<ICollection<ContactView>> GetBySurnameAsync(string surname)
        {
            var contacts = await _contactRepository.GetBySurnameAsync(surname);
            return MapContactViews(contacts);
        }
        public ICollection<ContactView> GetByTag(string tag)
        {
            var contacts = _contactRepository.GetByTag(tag);
            return MapContactViews(contacts);
        }
        public async Task<ICollection<ContactView>> GetByTagAsync(string tag)
        {
            var contacts = await _contactRepository.GetByTagAsync(tag);
            return MapContactViews(contacts);
        }

        public async Task<ContactView> GetContactAsync(int contactId)
        {
            var contact = await _contactRepository.GetContactAsync(contactId);
            return new ContactView(
                        contact.PersonId,
                        contact.Name,
                        contact.Surname,
                        contact.Address,
                        contact.Nickname,
                        _tagMap.GetTagsByContact(contact.PersonId),
                        _phoneNumberMap.GetAllPhoneNumbersByContact(contact.PersonId),
                        _mailMap.GetAllByContactId(contact.PersonId),
                        contact.IsBookmarked);
        }

        public void PostContact(ContactView contactView)
        {
            var contactModel = _contactRepository.AddContact(contactView.Name,contactView.Surname,contactView.Nickname,contactView.Address);
            contactView.ContactId = contactModel.PersonId;
            foreach (var contactViewMail in contactView.Mails)
            {
                if(contactViewMail.Email != "")
                    _mailMap.PostMail(contactView.ContactId,contactViewMail);
            }

            foreach (var contactViewPhoneNumber in contactView.PhoneNumbers)
            {
                if(contactViewPhoneNumber.Number != "")
                    _phoneNumberMap.PostPhoneNumber(contactView.ContactId,contactViewPhoneNumber);
            }

            foreach (var contactViewTag in contactView.Tags)
            {
                if(contactViewTag.Group != "")
                    _tagMap.PostTagToContact(contactView.ContactId, contactViewTag);
            }
        }
        public async Task PostContactAsync(ContactView contactView)
        {
            var contactModel = await _contactRepository.AddContactAsync(contactView.Name, contactView.Surname, contactView.Nickname, contactView.Address);
            contactView.ContactId = contactModel.PersonId;
            foreach (var contactViewMail in contactView.Mails)
            {
                if (contactViewMail.Email != "")
                    await _mailMap.PostMailAsync(contactView.ContactId, contactViewMail);
            }

            foreach (var contactViewPhoneNumber in contactView.PhoneNumbers)
            {
                if (contactViewPhoneNumber.Number != "")
                    await _phoneNumberMap.PostPhoneNumberAsync(contactView.ContactId, contactViewPhoneNumber);
            }

            foreach (var contactViewTag in contactView.Tags)
            {
                if (contactViewTag.Group != "")
                    await _tagMap.PostTagToContactAsync(contactView.ContactId, contactViewTag);
            }
        }

        public async Task<ContactView> ToggleBookmarkAsync(int contactId)
        {
            await _contactRepository.ToggleBookmarkAsync(contactId);
            return await GetContactAsync(contactId);
        }
        public void PutContact(int contactId, ContactView contactView)
        {
            _contactRepository.EditContact(contactId, contactView.Name, contactView.Surname, contactView.Nickname, contactView.Address);
            _mailMap.PutRange(contactView.ContactId,contactView.Mails);
            _phoneNumberMap.PutRange(contactView.ContactId, contactView.PhoneNumbers);
            _tagMap.PostTagToContactRange(contactView.ContactId, contactView.Tags);
        }
        public async Task PutContactAsync(int contactId, ContactView contactView)
        {
            await _contactRepository.EditContactAsync(contactId, contactView.Name, contactView.Surname, contactView.Nickname, contactView.Address);
            await _mailMap.PutRangeAsync(contactView.ContactId, contactView.Mails);
            await _phoneNumberMap.PutRangeAsync(contactView.ContactId, contactView.PhoneNumbers);
            await _tagMap.PostTagToContactRangeAsync(contactView.ContactId, contactView.Tags);
        }

        public void DeleteContact(int contactId)
        {
            _contactRepository.RemoveContact(contactId);
        }

        public async Task DeleteContactAsync(int contactId)
        {
            await _contactRepository.RemoveContactAsync(contactId);
        }
    }
}
