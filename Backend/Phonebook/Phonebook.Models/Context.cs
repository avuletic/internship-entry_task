﻿using Microsoft.EntityFrameworkCore;
using Phonebook.Models.Entities;

namespace Phonebook.Models
{
    public class Context : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<ContactMail> ContactMails { get; set; }
        public DbSet<ContactPhoneNumber> ContactPhoneNumbers { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<ContactTag> ContactTags { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=Phonebook;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContactTag>()
                .HasOne(cntTag => cntTag.Contact)
                .WithMany(cntTag => cntTag.ContactTags)
                .HasForeignKey(cntTag => cntTag.ContactId);
            modelBuilder.Entity<ContactTag>()
                .HasOne(cntTag => cntTag.Tag)
                .WithMany(cntTag => cntTag.ContactTags)
                .HasForeignKey(cntTag => cntTag.TagId);
        }
    }
}
