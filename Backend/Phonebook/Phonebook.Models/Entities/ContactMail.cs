﻿namespace Phonebook.Models.Entities
{
    public class ContactMail
    {
        public int ContactMailId { get; set; }
        public string Email { get; set; }
        public Contact Contact { get; set; }
        public ContactMail()
        {
        }
        public ContactMail(string email, Contact contact)
        {
            Email = email;
            Contact = contact;
        }
    }
}
