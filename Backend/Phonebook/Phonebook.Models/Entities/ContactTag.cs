﻿namespace Phonebook.Models.Entities
{
    public class ContactTag
    {
        public int ContactTagId { get; set; }
        public int ContactId { get; set; }
        public Contact Contact { get; set; }
        public int TagId { get; set; }
        public Tag Tag { get; set; }

        public ContactTag()
        {
        }

        public ContactTag(Contact contact, Tag tag)
        {
            Contact = contact;
            Tag = tag;
        }
    }
}
