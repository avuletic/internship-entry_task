﻿namespace Phonebook.Models.Entities
{
    public class ContactPhoneNumber
    {
        public int ContactPhoneNumberId { get; set; }
        public string PhoneNumber { get; set; }
        public Contact Contact { get; set; }
        public ContactPhoneNumber()
        {
        }
        public ContactPhoneNumber(string phoneNumber, Contact contact)
        {
            PhoneNumber = phoneNumber;
            Contact = contact;
        }
    }
}
