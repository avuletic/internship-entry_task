﻿using System.Collections.Generic;

namespace Phonebook.Models.Entities
{
    public class Contact : Person
    {
        public int ContactId { get; set; }
        public string Nickname { get; set; }
        public bool IsBookmarked { get; set; }
        public ICollection<ContactTag> ContactTags { get; set; }
        public ICollection<ContactMail> ContactMails { get; set; }
        public ICollection<ContactPhoneNumber> ContactPhoneNumbers { get; set; }
        public Contact()
        {
        }
        public Contact(string nickname, string name, string surname)
            : base(name, surname)
        {
            Nickname = nickname;
            IsBookmarked = false;
        }
        public Contact(string nickname, string name, string surname, string address)
            : base(name, surname, address)
        {
            Nickname = nickname;
            IsBookmarked = false;
        }
    }
}
