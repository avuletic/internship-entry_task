﻿namespace Phonebook.Models.Entities
{
    public class Person
    {
        public int PersonId { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }

        public Person()
        {

        }
        public Person(string name, string surname, string address= "")
        {
            Name = name;
            Surname = surname;
            Address = address;
        }
    }
}
