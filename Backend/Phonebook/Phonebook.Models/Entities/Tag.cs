﻿using System.Collections.Generic;

namespace Phonebook.Models.Entities
{
    public class Tag
    {
        public int TagId { get; set; }
        public string Group { get; set; }
        public ICollection<ContactTag> ContactTags { get; set; }
        public Tag()
        {
        }
        public Tag(string group)
        {
            Group = group;
        }
    }
}
