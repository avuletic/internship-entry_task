﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Phonebook.Models.Migrations
{
    public partial class RemovingEnum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Gender",
                table: "Persons");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Gender",
                table: "Persons",
                nullable: false,
                defaultValue: 0);
        }
    }
}
