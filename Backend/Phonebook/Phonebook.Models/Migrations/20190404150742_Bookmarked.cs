﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Phonebook.Models.Migrations
{
    public partial class Bookmarked : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsBookmarked",
                table: "Persons",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsBookmarked",
                table: "Persons");
        }
    }
}
