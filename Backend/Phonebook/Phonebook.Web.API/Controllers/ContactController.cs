﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Phonebook.Maps;
using Phonebook.ViewModel;

namespace Phonebook.Web.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    public class ContactController : ControllerBase
    {
        private readonly ContactMap _contactMap;
        public ContactController()
        {
            _contactMap = new ContactMap();
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ContactView>>> Get()
        {
            var listOfContacts = await _contactMap.GetAllAsync();
            return listOfContacts.ToList();
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ContactView>>> GetBookmarked()
        {
            var listOfBookmarkedContacts = await _contactMap.GetAllBookmarkedAsync();
            return listOfBookmarkedContacts.ToList();
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<ContactView>> Get(int id)
        {
            return await _contactMap.GetContactAsync(id);
        }

        [HttpGet("{name}")]
        public async Task<ActionResult<IEnumerable<ContactView>>> GetByName(string name)
        {
            var listOfContactsContainingName  = await _contactMap.GetByNameAsync(name);
            return listOfContactsContainingName.ToList();
        }
        [HttpGet("{surname}")]
        public async Task<ActionResult<IEnumerable<ContactView>>> GetBySurname(string surname)
        {
            var listOfContactsContainingSurname = await _contactMap.GetBySurnameAsync(surname);
            return listOfContactsContainingSurname.ToList();
        }
        [HttpGet("{tag}")]
        public async Task<ActionResult<IEnumerable<ContactView>>> GetByTag(string tag)
        {
            var listOfContactsContainingTags = await _contactMap.GetByTagAsync(tag);
            return listOfContactsContainingTags.ToList();
        }
        [HttpPost]
        public async Task<ActionResult<ContactView>> Post([FromBody] ContactView contact)
        {
            await _contactMap.PostContactAsync(contact);
            return contact;
        }

        [HttpPost]
        public async Task<ActionResult<ContactView>> PostToggleBookmark([FromBody] ContactView contactView)
        {
            return await _contactMap.ToggleBookmarkAsync(contactView.ContactId);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ContactView>> Put(int id,[FromBody] ContactView contact)
        {
            await _contactMap.PutContactAsync(id, contact);
            return contact;
        }

        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _contactMap.DeleteContactAsync(id);
        }

    }
}